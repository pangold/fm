require 'test_helper'

class LicenseHistoriesControllerTest < ActionController::TestCase
  setup do
    @license_history = license_histories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:license_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create license_history" do
    assert_difference('LicenseHistory.count') do
      post :create, license_history: { description: @license_history.description, firmware_id: @license_history.firmware_id, license_id: @license_history.license_id, operater_id: @license_history.operater_id, product_id: @license_history.product_id, scheme_id: @license_history.scheme_id }
    end

    assert_redirected_to license_history_path(assigns(:license_history))
  end

  test "should show license_history" do
    get :show, id: @license_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @license_history
    assert_response :success
  end

  test "should update license_history" do
    patch :update, id: @license_history, license_history: { description: @license_history.description, firmware_id: @license_history.firmware_id, license_id: @license_history.license_id, operater_id: @license_history.operater_id, product_id: @license_history.product_id, scheme_id: @license_history.scheme_id }
    assert_redirected_to license_history_path(assigns(:license_history))
  end

  test "should destroy license_history" do
    assert_difference('LicenseHistory.count', -1) do
      delete :destroy, id: @license_history
    end

    assert_redirected_to license_histories_path
  end
end
