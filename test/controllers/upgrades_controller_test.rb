require 'test_helper'

class UpgradesControllerTest < ActionController::TestCase
  setup do
    @upgrade = upgrades(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:upgrades)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create upgrade" do
    assert_difference('Upgrade.count') do
      post :create, upgrade: { attach_id: @upgrade.attach_id, description: @upgrade.description, firmware_version: @upgrade.firmware_version, name: @upgrade.name, scheme_version: @upgrade.scheme_version, status: @upgrade.status, tool: @upgrade.tool }
    end

    assert_redirected_to upgrade_path(assigns(:upgrade))
  end

  test "should show upgrade" do
    get :show, id: @upgrade
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @upgrade
    assert_response :success
  end

  test "should update upgrade" do
    patch :update, id: @upgrade, upgrade: { attach_id: @upgrade.attach_id, description: @upgrade.description, firmware_version: @upgrade.firmware_version, name: @upgrade.name, scheme_version: @upgrade.scheme_version, status: @upgrade.status, tool: @upgrade.tool }
    assert_redirected_to upgrade_path(assigns(:upgrade))
  end

  test "should destroy upgrade" do
    assert_difference('Upgrade.count', -1) do
      delete :destroy, id: @upgrade
    end

    assert_redirected_to upgrades_path
  end
end
