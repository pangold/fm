class User < ActiveRecord::Base
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # This is used to add the login for allowing user to sign in with both email or username
  attr_accessor :login
  has_one :users_role

  # Validations
  validates :username, presence: true, length: { maximum: 255}, uniqueness: { case_sensitive: false }
  validates_format_of :username, with: /^[a-zA-Z0-9_\.]*$/, multiline: true
  validate :validate_username

  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def self.current_user
    Thread.current[:current_user]
  end

  def self.current_user=(usr)
    Thread.current[:current_user] = usr
  end
  
  # callback
  before_create :ensure_authentication_token
   
  def validate_username
    if User.where(email: username).exists?
      errors.add(:username, :invalid)
    end
  end

  #token为空时自动生成新的token
  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def reset_authentication_token!
    generate_authentication_token
    self.save
  end

  # Overwrite devise’s find_for_database_authentication method in user model
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    elsif conditions.has_key?(:username) || conditions.has_key?(:email)
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: condition[:username]).first
      end
    end
  end

  
  def stored_create_log
    ary = []
    ary << "用户名：#{self.username}" if self.username
    ary << "Email：#{self.email}" if self.email
    ary << "昵称：#{self.name}" if self.name
     
    if ary.size > 0
      title = "添加用户"
      content = ary * ", "
      Syslog.stored_log(title, content, self.id)
    end
  end

  def stored_update_log
    old = User.find(self.id)
    ary = []
    ary << "用户名：#{old.username} -> #{self.username}" if old.username != self.username
    ary << "Email：#{old.email} -> #{self.email}" if old.email != self.email
    ary << "昵称：#{old.name} -> #{self.name}" if old.name != self.name
    
    ary2 = []
    ary2 << "时间：#{self.current_sign_in_at}" if old.current_sign_in_at != self.current_sign_in_at
    ary2 << "IP: #{self.current_sign_in_ip}" if old.current_sign_in_ip != self.current_sign_in_ip
   
    ary3 = []
    ary3 << "重置了密码" if old.encrypted_password != self.encrypted_password
     
    if ary.size > 0
      title = "修改用户信息"
      content = "[#{old.username}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
    
    if ary2.size > 0
      title = "登录系统"
      content = "[#{old.username}] " + ary2 * ", "
      Syslog.stored_log(title, content, self.id)
    end
    
    if ary3.size > 0
      title = "修改密码"
      content = "[#{old.username}] " + ary3 * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除用户", "用户名：#{self.username}，昵称：#{self.name}，Email: #{self.email}", self.id)
  end
end
