class License < ActiveRecord::Base
  FLAG_TYPE = { "未使用" => 0, "已使用" => 1, "已作废" => -1 }
  
  # auto show their license that according to difference factories or customers
  # and the license must be in unused status.
  # and just return one data.
  def self.find_automatical(code, point)
    return self.find_by_sql("SELECT * FROM licenses WHERE licenses.snid LIKE '#{code}%' AND licenses.point=#{point} AND licenses.flag=0 ORDER BY licenses.created_at desc LIMIT 1")
  end
  
  def has_used
    self.flag = 1
    self.save
  end

  def has_nullified
    self.flag = -1
    self.save
  end
  
  def unused
    self.flag = 0
    self.save
  end

  def has_userd?
    return self.flag == 1
  end

  def has_nullified? 
    return self.flag == -1
  end

  def unused?
    return self.flag == 0
  end
end
