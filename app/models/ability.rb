class Ability
  include CanCan::Ability

  def initialize(user)
    if user.blank?
      cannot :manage, :all
      cannot :read, :all
    elsif user.has_role?(:admin)
      can :manage, :all
    elsif user.has_role?(:manager)
      basic_manage
    else
      basic_read_only
    end
  end


  protected
  
  def basic_read_only
    can :read, [Product, Firmware, Scheme, Upgrade, Tool, Category, Solution, Protocol]
    can :read, [ProductHistory, BurnHistory, LicenseHistory]
  end

  def basic_manage
    can :manage, [Product, Firmware, Scheme, Upgrade, Tool, Category, Solution, Protocol, LicenseCode, License]
    can :manage, [ProductHistory, BurnHistory, LicenseHistory]
    can :read, :all
  end
end
