class BurnHistory < ActiveRecord::Base
  belongs_to :operater, class_name: "User"
  belongs_to :product
  belongs_to :firmware
  belongs_to :scheme

  after_create :stored_create_log

  def stored_create_log
    ary = []
    ary << "产品：#{self.product.model}" if self.product
    ary << "固件：#{self.firmware.name}" if self.firmware
    ary << "配置：#{self.scheme.name}" if self.scheme
    ary << "操作人：#{self.operater.name}" if self.operater
    
    if ary.size > 0
      title = "烧写固件配置"
      content = "[#{self.product.model}] " + ary * ", "
      Syslog.stored_log(title, content, self.operater_id)
    end
  end
end
