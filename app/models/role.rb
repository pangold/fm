class Role < ActiveRecord::Base
  has_and_belongs_to_many :users, :join_table => :users_roles
  belongs_to :resource, :polymorphic => true

  validates :resource_type,
            :inclusion => { :in => Rolify.resource_types },
            :allow_nil => true

  scopify

  
  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def stored_create_log
    Syslog.stored_log("添加角色", "名称：#{self.name}")
  end

  def stored_update_log
    old = Role.find(self.id)
    ary = []
    ary << "名称：#{old.name} -> #{self.name}" if old.name != self.name
    
    if ary.size > 0
      title = "修改角色"
      content = "[#{old.name}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除角色", "名称：#{self.name}")
  end
end
