class Tool < ActiveRecord::Base
  belongs_to :attach

  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def stored_create_log
    Syslog.stored_log("添加工具", "名称：#{self.name}，版本：#{self.version}")
  end

  def stored_update_log
    old = Tool.find(self.id)
    ary = []
    ary << "名称：#{old.name} -> #{self.name}" if old.name != self.name
    ary << "版本：#{old.version} -> #{self.version}" if old.version != self.version
    ary << "描述：#{old.description} -> #{self.description}" if old.description != self.description
    
    if ary.size > 0
      title = "修改工具"
      content = "[#{old.name}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除工具", "名称：#{self.name}")
  end
end
