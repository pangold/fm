class Attach < ActiveRecord::Base
  after_create :stored_create_log
  before_destroy :stored_delete_log

  def initialize(upload, session_id, dir = "uploads")
    file_upload(upload, session_id, dir)
    super()
    # self.user_id = session_id
    self.file_name = @file_name
    self.file_type = @file_type
    self.extension = @extension
    self.save_name = @save_name
    self.save_path = @save_path
    self.size      = @file_size
    self.file_hash = @file_hash
    self.file_url  = @file_url
    self.download_count = 0
    self.original_name = @original_name
  end

  def add_download_count
    self.download_count += 1
    self.save
  end

  def stored_create_log
    Syslog.stored_log("上传文件", "名称：#{self.original_name}，大小：#{self.size / 1024 + 1} KB，URL: #{self.file_url}，HASH: #{self.file_hash}")
  end

  def stored_delete_log
    Syslog.stored_log("删除文件", "名称：#{self.original_name}")
  end


  protected

  def file_upload(upload, session_id, dir = "uploads")
    time_now = Time.now
    @init_dir = dir
    begin
      FileUtils.mkpath(upload_path) unless File.directory?(upload_path)
      if upload.kind_of?(StringIO)
        upload.rewind
      end

      original_name = upload.original_filename
      base_name = File.basename(original_name, ".*")
      extension_name = File.extname(original_name).downcase
      tmp_name = "_1"
      tmp_extension_name = "_" + time_now.to_i.to_s + extension_name

      # rename
      base_name = session_id.nil? ? Digest::MD5.hexdigest(original_name).to_s : session_id.to_s
      base_name = base_name.upcase
      end_name = base_name + tmp_extension_name

      # no using a exist name
      while File.exist?(upload_path(end_name))
        arr = end_name.scan(/[a-zA-Z0-9_]+/)[0].split("_")
        end_name = 2 == arr.size ? (base_name + tmp_name + tmp_extension_name) : (base_name + "_" + arr[1].succ.to_s + tmp_extension_name)
      end

      # writen in server's hard disk
      File.open(upload_path(end_name), "wb") {|f| f.write(upload.read)}
      saveurl = end_name
      save_name = end_name

      @original_name = original_name
      @file_name = File.basename(original_name, ".*")
      @file_type = extension_name.delete(".") # ???
      @extension = extension_name.delete(".")
      @save_name = save_name
      @save_path = upload_path
      @file_size = upload.size
      @file_hash = Digest::MD5.hexdigest(original_name)
      @file_url  = @init_dir + "/" + @save_name

    rescue
      raise
    end
  end

  def upload_path(file = nil)
    "#{Rails.root}/public/#{@init_dir}/#{file.nil? ? '' : file}"
  end

end
