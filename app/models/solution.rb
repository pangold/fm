class Solution < ActiveRecord::Base
  after_create :stored_create_log
  before_update :stored_update_log
  before_destroy :stored_delete_log

  def stored_create_log
    Syslog.stored_log("添加方案", "名称：#{self.name}，描述：#{self.description}")
  end

  def stored_update_log
    old = Solution.find(self.id)
    ary = []
    ary << "名称：#{old.name} -> #{self.name}" if old.name != self.name
    ary << "描述：#{old.description} -> #{self.description}" if old.description != self.description
    
    if ary.size > 0
      title = "修改方案"
      content = "[#{old.name}] " + ary * ", "
      Syslog.stored_log(title, content)
    end
  end

  def stored_delete_log
    Syslog.stored_log("删除方案", "名称：#{self.name}")
  end
end
