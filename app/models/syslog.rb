class Syslog < ActiveRecord::Base
  belongs_to :operater, class_name: "User"

  def self.stored_log(title, description, uid = nil)
    sys = Syslog.new
    sys.operater_id = User.current_user.id if User.current_user
    sys.operater_id = uid if uid
    sys.title = title
    sys.description = description
    # sys.ip = @env["HTTP_X_FORWARDED_FOR"] # @env['HTTP_CLIENT_IP']
    sys.save
  end
end
