json.array!(@licenses) do |license|
  json.extract! license, :id, :snid, :sn, :point, :flag, :status
  json.url license_url(license, format: :json)
end
