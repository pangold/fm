json.array!(@product_histories) do |product_history|
  json.extract! product_history, :id, :description, :status, :operater_id, :examiner_id, :product_id, :firmware_id, :scheme_id, :upgrade_id, :tool_id
  json.url product_history_url(product_history, format: :json)
end
