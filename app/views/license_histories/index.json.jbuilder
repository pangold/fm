json.array!(@license_histories) do |license_history|
  json.extract! license_history, :id, :description, :license_id, :operater_id, :product_id, :firmware_id, :scheme_id
  json.url license_history_url(license_history, format: :json)
end
