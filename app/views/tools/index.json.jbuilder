json.array!(@tools) do |tool|
  json.extract! tool, :id, :name, :version, :description, :status, :attach_id
  json.url tool_url(tool, format: :json)
end
