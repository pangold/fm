json.array!(@burn_histories) do |burn_history|
  json.extract! burn_history, :id, :description, :operater_id, :product_id, :firmware_id, :scheme_id
  json.url burn_history_url(burn_history, format: :json)
end
