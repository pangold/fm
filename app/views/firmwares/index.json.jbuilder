json.array!(@firmwares) do |firmware|
  json.extract! firmware, :id, :name, :version, :description, :status, :attach_id
  json.url firmware_url(firmware, format: :json)
end
