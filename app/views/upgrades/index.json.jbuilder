json.array!(@upgrades) do |upgrade|
  json.extract! upgrade, :id, :name, :firmware_version, :scheme_version, :tool, :description, :status, :attach_id
  json.url upgrade_url(upgrade, format: :json)
end
