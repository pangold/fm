json.array!(@protocols) do |protocol|
  json.extract! protocol, :id, :name, :description, :status
  json.url protocol_url(protocol, format: :json)
end
