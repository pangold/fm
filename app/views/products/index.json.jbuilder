json.array!(@products) do |product|
  json.extract! product, :id, :name, :model, :place, :param, :custom, :status, :category_id, :protocol_id, :solution_id, :firmware_id, :scheme_id, :upgrade_id, :tool_id
  json.url product_url(product, format: :json)
end
