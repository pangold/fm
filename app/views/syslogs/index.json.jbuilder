json.array!(@syslogs) do |syslog|
  json.extract! syslog, :id, :title, :description, :ip, :operater_id
  json.url syslog_url(syslog, format: :json)
end
