class ToolsController < ApplicationController
  authorize_resource
  before_action :set_tool, only: [:show, :edit, :update, :destroy]
  before_action :uploading, only: [:create, :update]
  after_action :stored_download_log, only: [:download]

  # GET /tools
  # GET /tools.json
  def index
    @tools = Tool.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /tools/1
  # GET /tools/1.json
  def show
  end

  # GET /tools/new
  def new
    @tool = Tool.new
  end

  # GET /tools/1/edit
  def edit
  end

  # POST /tools
  # POST /tools.json
  def create
    @tool = Tool.new(tool_params)

    respond_to do |format|
      if @tool.save
        format.html { redirect_to @tool, notice: 'Tool was successfully created.' }
        format.json { render :show, status: :created, location: @tool }
      else
        format.html { render :new }
        format.json { render json: @tool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tools/1
  # PATCH/PUT /tools/1.json
  def update
    respond_to do |format|
      if @tool.update(tool_params)
        format.html { redirect_to @tool, notice: 'Tool was successfully updated.' }
        format.json { render :show, status: :ok, location: @tool }
      else
        format.html { render :edit }
        format.json { render json: @tool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tools/1
  # DELETE /tools/1.json
  def destroy
    @tool.destroy
    respond_to do |format|
      format.html { redirect_to tools_url, notice: 'Tool was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Download tool file
  def download
    @tool = Tool.find(params[:id])
    if (!download_file @tool.attach)
      redirect_to tools_url, notice: 'No such file.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tool
      @tool = Tool.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tool_params
      params.require(:tool).permit(:name, :version, :description, :status, :attach_id)
    end
    
    def uploading
      upload = params[:tool][:upload_file]
      if upload && current_user
        @attach = Attach.new(upload, current_user[:id])
        if @attach.save
          params[:tool][:attach_id] = @attach.id
        end
      end
    end

    def stored_download_log
      ary = []
      ary << "工具：#{@tool.name}"
      ary << "名称：#{@tool.attach.original_name}" if @tool.attach
      ary << "URL：#{@tool.attach.file_url}" if @tool.attach
      ary << "HASH：#{@tool.attach.file_hash}" if @tool.attach
      ary << "大小：#{@tool.attach.size / 1024 + 1} KB" if @tool.attach
    
      if ary.size > 0
        title = "下载文件"
        content = ary * ", "
        Syslog.stored_log(title, content)
      end
    end
end
