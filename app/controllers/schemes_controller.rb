class SchemesController < ApplicationController
  authorize_resource
  before_action :set_scheme, only: [:show, :edit, :update, :destroy]
  before_action :uploading, only: [:create, :update]
  after_action :stored_download_log, only: [:download]

  # GET /schemes
  # GET /schemes.json
  def index
    @schemes = Scheme.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /schemes/1
  # GET /schemes/1.json
  def show
  end

  # GET /schemes/new
  def new
    @scheme = Scheme.new
  end

  # GET /schemes/1/edit
  def edit
  end

  # POST /schemes
  # POST /schemes.json
  def create
    @scheme = Scheme.new(scheme_params)

    respond_to do |format|
      if @scheme.save
        format.html { redirect_to @scheme, notice: 'Scheme was successfully created.' }
        format.json { render :show, status: :created, location: @scheme }
      else
        format.html { render :new }
        format.json { render json: @scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schemes/1
  # PATCH/PUT /schemes/1.json
  def update
    respond_to do |format|
      if @scheme.update(scheme_params)
        format.html { redirect_to @scheme, notice: 'Scheme was successfully updated.' }
        format.json { render :show, status: :ok, location: @scheme }
      else
        format.html { render :edit }
        format.json { render json: @scheme.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schemes/1
  # DELETE /schemes/1.json
  def destroy
    @scheme.destroy
    respond_to do |format|
      format.html { redirect_to schemes_url, notice: 'Scheme was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Download scheme file
  def download
    @scheme = Scheme.find(params[:id])
    if (!download_file @scheme.attach)
      redirect_to schemes_url, notice: 'No such file.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_scheme
      @scheme = Scheme.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def scheme_params
      params.require(:scheme).permit(:name, :version, :description, :status, :attach_id)
    end

    def uploading
      upload = params[:scheme][:upload_file]
      if upload && current_user
        @attach = Attach.new(upload, current_user[:id])
        if @attach.save
          params[:scheme][:attach_id] = @attach.id
        end
      end
    end

    def stored_download_log
      ary = []
      ary << "配置文件：#{@scheme.name}"
      ary << "名称：#{@scheme.attach.original_name}" if @scheme.attach
      ary << "URL：#{@scheme.attach.file_url}" if @scheme.attach
      ary << "HASH：#{@scheme.attach.file_hash}" if @scheme.attach
      ary << "大小：#{@scheme.attach.size / 1024 + 1} KB" if @scheme.attach
    
      if ary.size > 0
        title = "下载文件"
        content = ary * ", "
        Syslog.stored_log(title, content)
      end
    end
end
