class Api::V1::LicenseCodesController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    license_codes = LicenseCode.all
    render json: ActiveModel::ArraySerializer.new(license_codes, each_serializer: Api::V1::LicenseCodeSerializer, root: 'license_codes')
  end

  def show
    license_code = LicenseCode.find(params[:id])
    render json: Api::V1::LicenseCodeSerializer.new(license_code).to_json
  end
end
