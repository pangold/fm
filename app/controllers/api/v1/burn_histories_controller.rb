class Api::V1::BurnHistoriesController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    burn_histories = BurnHistory.all
    render json: ActiveModel::ArraySerializer.new(burn_histories, each_serializer: Api::V1::BurnHistorySerializer, root: 'burn_histories')
  end

  def show
    burn_history = BurnHistory.find(params[:id])
    render json: Api::V1::BurnHistorySerializer.new(burn_history).to_json
  end

  def create
    burn_history = BurnHistory.new(create_params)
    if burn_history.save
      render json: Api::V1::BurnHistorySerializer.new(burn_history).to_json, status: 201
    else
      return api_error status: 204
    end
  end


  private

  def create_params
    params.require(:burn_history).permit(:operater_id, :product_id, :firmware_id, :scheme_id)
  end
end
