class Api::V1::LicenseHistoriesController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    license_histories = LicenseHistory.all
    render json: ActiveModel::ArraySerializer.new(license_histories, each_serializer: Api::V1::LicenseHistorySerializer, root: 'license_histories')
  end

  def show
    license_history = LicenseHistory.find(params[:id])
    render json: Api::V1::LicenseHistorySerializer.new(license_history).to_json
  end

  def create
    license_history = LicenseHistory.new(create_params)
    if license_history.save
      license_history.license.has_used
      render json: Api::V1::LicenseHistorySerializer.new(license_history).to_json, status: 201
    else
      return api_error status: 204
    end
  end


  private

  def create_params
    params.require(:license_history).permit(:operater_id, :product_id, :license_id, :firmware_id, :scheme_id)
  end
end
