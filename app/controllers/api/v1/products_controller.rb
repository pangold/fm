class Api::V1::ProductsController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    products = Product.all
    render json: ActiveModel::ArraySerializer.new(products, each_serializer: Api::V1::ProductSerializer, root: 'products')
  end

  def show
    product = Product.find(params[:id])
    render json: Api::V1::ProductSerializer.new(product).to_json
  end

  def model
    product = Product.find_by_model(params[:model])
    if product
      render json: Api::V1::ProductSerializer.new(product).to_json
    else
      return api_error status: 404
    end
  end
end
