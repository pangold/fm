class Api::V1::LicensesController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    licenses = License.all
    render json: ActiveModel::ArraySerializer.new(licenses, each_serializer: Api::V1::LicenseSerializer, root: 'licenses')
  end

  # we can only use the license which flag in unused status
  def show
    license = License.find(params[:id])
    if license && license.unused?
      render json: Api::V1::LicenseSerializer.new(license).to_json
    else
      return api_error status: 404
    end
  end

  # we can only use the license which flag in unused status
  def snid
    # license = License.find_by_snid(params[:snid])
    if params[:point]
      license = License.where("snid = ? AND point = ?", params[:snid], params[:point]).first
    else
      license = License.find_by_snid(params[:snid])
    end
    if license && license.unused?
      render json: Api::V1::LicenseSerializer.new(license).to_json
    else
      return api_error status: 404
    end
  end

  # auto show their license that according to difference factories or customers
  # and the license must be in unused status.
  # then, the license table has no way to judge which customer the license is.
  # so, it's a column need to be add in license database table.
  # or using like here: where snid like 900%
  def auto
    license = License.find_automatical(params[:code], params[:point]).first
    if license && license.unused?
      render json: Api::V1::LicenseSerializer.new(license).to_json
    else
      return api_error status: 404
    end
  end
end
