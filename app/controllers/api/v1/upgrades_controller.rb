class Api::V1::UpgradesController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    upgrades = Upgrade.all
    render json: ActiveModel::ArraySerializer.new(upgrades, each_serializer: Api::V1::UpgradeSerializer, root: 'upgrades')
  end

  def show
    upgrade = Upgrade.find(params[:id])
    render json: Api::V1::UpgradeSerializer.new(upgrade).to_json
  end
end
