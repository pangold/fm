class Api::V1::FirmwaresController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    firmwares = Firmware.all
    render json: ActiveModel::ArraySerializer.new(firmwares, each_serializer: Api::V1::FirmwareSerializer, root: 'firmwares')
  end

  def show
    firmware = Firmware.find(params[:id])
    render json: Api::V1::FirmwareSerializer.new(firmware).to_json
  end
end
