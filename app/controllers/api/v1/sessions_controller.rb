class Api::V1::SessionsController < Api::V1::BaseController
  def create
    user = User.find_by(username: create_params[:username])
    if user && user.valid_password?(create_params[:password]) 
      self.current_user = user
      render json: Api::V1::SessionSerializer.new(user).to_json, status: 201
      # return api_error status: 201
    else
      return api_error status: 401
    end
  end

  # 注销就是更换用户token
  def destroy
    self.current_user.authentication_token = Devise.friendly_token
    sign_out(current_user)
    render json: {success: true}
  end


  private

  def create_params
    params.require(:user).permit(:username, :password)
  end
end

