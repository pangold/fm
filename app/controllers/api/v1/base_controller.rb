class Api::V1::BaseController < ApplicationController
  attr_accessor :current_user

  # disable the CSRF token
  protect_from_forgery with: :null_session
  rescue_from ActiveRecord::RecordNotFound, with: :not_found

  # disable cookie (no set-cookies header in response)
  before_action :destroy_session
  
  # disable the CSRF token
  skip_before_action :verify_authenticity_token
  
  def destroy_session
    request.session_options[:skip] = true
  end

  def api_error(opts = {})
    render nothing: true, status: opts[:status]
  end

  def unauthenticated!
    api_error(status: 401)
  end

  ## 需要cookie认证
  def authenticate_user!
    token, eptions = ActionControl er::HttpAuthentication::Token.token_and_options(request)
    user_email = options.blank?? nil : options[:email]
    user = user_email && User.find_by(email: user_email)

    if user && ActiveSupport::SecurityUtils.secure_compare(user.authentication_token, token)
      self.current_user = user
    else
      return unauthenticated!
    end
  end

  def not_found
    return api_error(status: 404, errors: 'Not found')
  end

  ##需要token认证
  # 获取http:/xxx.com/books.json?token=aMUj5kiyLbmZdjpr_iAu
  # 判断token的值是否存在，若存在且能在User表中找到相应的，就登录此用户
  def authenticate_user_from_token!
    token = params[:token].presence
    user = token && User.find_by_authentication_token(token.to_s)
    if user
      return api_error status: 401
    end
  end
end
