class Api::V1::SchemesController < Api::V1::BaseController
  before_filter :authenticate_user_from_token!

  def index
    schemes = Scheme.all
    render json: ActiveModel::ArraySerializer.new(schemes, each_serializer: Api::V1::SchemeSerializer, root: 'schemes')
  end

  def show
    scheme = Scheme.find(params[:id])
    render json: Api::V1::SchemeSerializer.new(scheme).to_json
  end
end
