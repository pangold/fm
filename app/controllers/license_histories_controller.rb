class LicenseHistoriesController < ApplicationController
  authorize_resource
  before_action :set_license_history, only: [:show, :edit, :update, :destroy]

  # GET /license_histories
  # GET /license_histories.json
  def index
    @license_histories = LicenseHistory.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /license_histories/1
  # GET /license_histories/1.json
  def show
  end

  # GET /license_histories/new
  def new
    @license_history = LicenseHistory.new
  end

  # GET /license_histories/1/edit
  def edit
  end

  # POST /license_histories
  # POST /license_histories.json
  def create
    @license_history = LicenseHistory.new(license_history_params)

    respond_to do |format|
      if @license_history.save
        format.html { redirect_to @license_history, notice: 'License history was successfully created.' }
        format.json { render :show, status: :created, location: @license_history }
      else
        format.html { render :new }
        format.json { render json: @license_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /license_histories/1
  # PATCH/PUT /license_histories/1.json
  def update
    respond_to do |format|
      if @license_history.update(license_history_params)
        format.html { redirect_to @license_history, notice: 'License history was successfully updated.' }
        format.json { render :show, status: :ok, location: @license_history }
      else
        format.html { render :edit }
        format.json { render json: @license_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /license_histories/1
  # DELETE /license_histories/1.json
  def destroy
    @license_history.destroy
    respond_to do |format|
      format.html { redirect_to license_histories_url, notice: 'License history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_license_history
      @license_history = LicenseHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def license_history_params
      params.require(:license_history).permit(:description, :license_id, :operater_id, :product_id, :firmware_id, :scheme_id)
    end
end
