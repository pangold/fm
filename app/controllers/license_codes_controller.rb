class LicenseCodesController < ApplicationController
  authorize_resource
  before_action :set_license_code, only: [:show, :edit, :update, :destroy]

  # GET /license_codes
  # GET /license_codes.json
  def index
    @license_codes = LicenseCode.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /license_codes/1
  # GET /license_codes/1.json
  def show
  end

  # GET /license_codes/new
  def new
    @license_code = LicenseCode.new
  end

  # GET /license_codes/1/edit
  def edit
  end

  # POST /license_codes
  # POST /license_codes.json
  def create
    @license_code = LicenseCode.new(license_code_params)

    respond_to do |format|
      if @license_code.save
        format.html { redirect_to @license_code, notice: 'License code was successfully created.' }
        format.json { render :show, status: :created, location: @license_code }
      else
        format.html { render :new }
        format.json { render json: @license_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /license_codes/1
  # PATCH/PUT /license_codes/1.json
  def update
    respond_to do |format|
      if @license_code.update(license_code_params)
        format.html { redirect_to @license_code, notice: 'License code was successfully updated.' }
        format.json { render :show, status: :ok, location: @license_code }
      else
        format.html { render :edit }
        format.json { render json: @license_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /license_codes/1
  # DELETE /license_codes/1.json
  def destroy
    @license_code.destroy
    respond_to do |format|
      format.html { redirect_to license_codes_url, notice: 'License code was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_license_code
      @license_code = LicenseCode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def license_code_params
      params.require(:license_code).permit(:code, :description, :status)
    end
end
