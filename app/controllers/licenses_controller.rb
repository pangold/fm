class LicensesController < ApplicationController
  require 'nokogiri'
  authorize_resource
  before_action :set_license, only: [:show, :edit, :update, :destroy]

  after_action :stored_create_log, only: [:create]
  before_action :stored_update_log, only: [:update]
  after_action :stored_delete_log, only: [:destroy]
  after_action :stored_invalid_log, only: [:invalid]
  after_action :stored_export_log, only: [:export]
  after_action :stored_import_log, only: [:import]
  after_action :stored_generate_log, only: [:generate]


  # GET /licenses
  # GET /licenses.json
  def index
    @licenses = License.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /licenses/1
  # GET /licenses/1.json
  def show
  end

  # GET /licenses/new
  def new
    @license = License.new
  end

  # GET /licenses/1/edit
  def edit
  end

  # POST /licenses
  # POST /licenses.json
  def create
    @license = License.new(license_params)

    respond_to do |format|
      if @license.save
        format.html { redirect_to @license, notice: 'License was successfully created.' }
        format.json { render :show, status: :created, location: @license }
      else
        format.html { render :new }
        format.json { render json: @license.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /licenses/1
  # PATCH/PUT /licenses/1.json
  def update
    respond_to do |format|
      if @license.update(license_params)
        format.html { redirect_to @license, notice: 'License was successfully updated.' }
        format.json { render :show, status: :ok, location: @license }
      else
        format.html { render :edit }
        format.json { render json: @license.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /licenses/1
  # DELETE /licenses/1.json
  def destroy
    @license.destroy
    respond_to do |format|
      format.html { redirect_to licenses_url, notice: 'License was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Invalid licenses
  def invalid
    @invalid_count = 0
    start_id = params[:start_id].upcase
    end_id = params[:end_id].succ.upcase
    while(start_id != end_id )
      license = License.find_by(snid: start_id)
      if license && license.flag != -1
        license.flag = -1
        if license.save
          @invalid_count += 1
        end
      end
      start_id = start_id.succ
    end
    redirect_to licenses_url, notice: 'Licenses were successfully invalided.' 
  end

  # Import licenses from xml
  def import
    @import_count = 0
    file = params[:import_file]
    xml = Nokogiri::XML.parse(file.read)
    xml.xpath('//licenses//license').each {|x|
      license = License.new
      license.snid = x.attributes["snid"].value
      license.sn = x.attributes["sn"].value
      license.point = x.attributes["point"].value
      license.status = 1
      license.flag = 0
      if license.save
        @import_count += 1
      end
    }
    redirect_to licenses_url, notice: 'Licenses were successfully imported.' 
  end

  # Export licenses to xml
  def export
    @export_count = 0
    start_id = params[:start_id].upcase
    end_id = params[:end_id].succ.upcase
    xml_doc = Nokogiri::XML("<root><licenses></licenses></root>")
    xml_licenses = xml_doc.at_css "licenses"
    while(start_id != end_id )
      license = License.find_by(snid: start_id)
      if license
        new_node = Nokogiri::XML::Node.new 'license', xml_doc
        att = {snid: license.snid, sn: license.sn, point: license.point}
        att.each {|k,v| new_node.set_attribute(k,v)}
        new_node.parent = xml_licenses
        @export_count += 1
      end
      start_id = start_id.succ
    end
    send_data(xml_doc.to_xml, filename: "licenses.xml", disposition: "attachment")
  end
 
  # Generate licenses which using our algorithm 
  def generate
    @generate_count = 0
    redirect_to licenses_url, notice: 'Licenses were successfully generated.' 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_license
      @license = License.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def license_params
      params.require(:license).permit(:snid, :sn, :point, :flag, :status)
    end
  
    def stored_create_log
      Syslog.stored_log("添加序列号", "ID: #{@license.snid}，点数：#{@license.point}")
    end

    def stored_update_log
      old = License.find(params[:id])
      ary = []
      ary << "ID: #{old.snid} -> #{license_params[:snid]}" if old.snid != license_params[:snid]
      ary << "SN: #{old.sn} -> #{license_params[:sn]}" if old.sn != license_params[:sn]
      ary << "点数：#{old.point} -> #{license_params[:point]}" if old.point.to_s != license_params[:point]
      ary << "状态：#{License::FLAG_TYPE.invert[old.flag]} -> #{License::FLAG_TYPE.invert[license_params[:flag].to_i]}" if old.flag.to_s != license_params[:flag]
    
      if ary.size > 0
        title = "修改序列号"
        content = "[#{old.snid}] " + ary * ", "
        Syslog.stored_log(title, content)
      end
    end

    def stored_delete_log
      Syslog.stored_log("删除序列号", "ID: #{@license.snid}")
    end
    
    def stored_invalid_log
      Syslog.stored_log("作废序列号", "作废数量：#{@invalid_count}")
    end
    
    def stored_import_log
      Syslog.stored_log("导入序列号", "导入数量：#{@import_count}")
    end
    
    def stored_export_log
      Syslog.stored_log("导出序列号", "导出数量：#{@export_count}")
    end
    
    def stored_generate_log
      Syslog.stored_log("生成序列号", "生成数量：#{@generate_count}")
    end
end
