class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
 
  before_filter :set_current_user 
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale

  # rescue_from ActionController::RoutingError, with: :render_error_404
  # rescue_from Mongoid::Errors::DocumentNotFound, with: :render_error_404

  # If have no permission, redirect to Home page
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  protected

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :username, :email, :password, :password_confirmation) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :username, :email, :password, :password_confirmation, :current_password) }
  end

  def render_error_404
    render file: "#{Rails.root}/public/404.html", status: 404, layout: false
  end 
  
  def set_current_user
    User.current_user = current_user
  end
 
  # Download firmware
  def download_file(attach)
    if (attach)
      down_name = attach.save_path + attach.save_name
      if File.exist?(down_name) 
        send_file(down_name, filename: attach.original_name, disposition: "attachment")
        attach.add_download_count
        return true
      end
    end
    return nil
  end

  def generate_log(title, description)
    syslog = Syslog.new
    syslog.operater_id = current_user[:id]
    syslog.title = title
    syslog.description = description
    syslog.ip = @env['HTTP_CLIENT_IP']
    syslog.save
  end

end
