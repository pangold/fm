class UpgradesController < ApplicationController
  authorize_resource
  before_action :set_upgrade, only: [:show, :edit, :update, :destroy]
  before_action :uploading, only: [:create, :update]
  after_action :stored_download_log, only: [:download]

  # GET /upgrades
  # GET /upgrades.json
  def index
    @upgrades = Upgrade.all.order(created_at: :desc).paginate(page:params[:page], per_page: 10)
  end

  # GET /upgrades/1
  # GET /upgrades/1.json
  def show
  end

  # GET /upgrades/new
  def new
    @upgrade = Upgrade.new
  end

  # GET /upgrades/1/edit
  def edit
  end

  # POST /upgrades
  # POST /upgrades.json
  def create
    @upgrade = Upgrade.new(upgrade_params)

    respond_to do |format|
      if @upgrade.save
        format.html { redirect_to @upgrade, notice: 'Upgrade was successfully created.' }
        format.json { render :show, status: :created, location: @upgrade }
      else
        format.html { render :new }
        format.json { render json: @upgrade.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /upgrades/1
  # PATCH/PUT /upgrades/1.json
  def update
    respond_to do |format|
      if @upgrade.update(upgrade_params)
        format.html { redirect_to @upgrade, notice: 'Upgrade was successfully updated.' }
        format.json { render :show, status: :ok, location: @upgrade }
      else
        format.html { render :edit }
        format.json { render json: @upgrade.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /upgrades/1
  # DELETE /upgrades/1.json
  def destroy
    @upgrade.destroy
    respond_to do |format|
      format.html { redirect_to upgrades_url, notice: 'Upgrade was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Download upgrade file
  def download
    @upgrade = Upgrade.find(params[:id])
    if (!download_file @upgrade.attach)
      redirect_to upgrades_url, notice: 'No such file.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_upgrade
      @upgrade = Upgrade.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def upgrade_params
      params.require(:upgrade).permit(:name, :firmware_version, :scheme_version, :tool, :description, :status, :attach_id)
    end

    def uploading
      upload = params[:upgrade][:upload_file]
      if upload && current_user
        @attach = Attach.new(upload, current_user[:id])
        if @attach.save
          params[:upgrade][:attach_id] = @attach.id
        end
      end
    end

    def stored_download_log
      ary = []
      ary << "升级包：#{@upgrade.name}"
      ary << "名称：#{@upgrade.attach.original_name}" if @upgrade.attach
      ary << "URL：#{@upgrade.attach.file_url}" if @upgrade.attach
      ary << "HASH：#{@upgrade.attach.file_hash}" if @upgrade.attach
      ary << "大小：#{@upgrade.attach.size / 1024 + 1} KB" if @upgrade.attach
    
      if ary.size > 0
        title = "下载文件"
        content = ary * ", "
        Syslog.stored_log(title, content)
      end
    end
end
