class Api::V1::LicenseSerializer < Api::V1::BaseSerializer
  attributes :id, :snid, :sn, :point, :flag
end
