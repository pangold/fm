class Api::V1::UpgradeSerializer < Api::V1::BaseSerializer
  attributes :id, :name, :firmware_version, :scheme_version, :attach
  
  has_one :attach
end
