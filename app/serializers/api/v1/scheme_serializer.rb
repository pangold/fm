class Api::V1::SchemeSerializer < Api::V1::BaseSerializer
  attributes :id, :name, :version, :attach
  
  has_one :attach
end
