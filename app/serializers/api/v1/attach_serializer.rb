class Api::V1::AttachSerializer < Api::V1::BaseSerializer
  attributes :id, :original, :name, :url
  
  def original
    object.original_name
  end
  
  def name
    object.save_name
  end

  def url
    object.file_url
  end

  def hash
    object.file_hash
  end
end
