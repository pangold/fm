class Api::V1::ProductSerializer < Api::V1::BaseSerializer
  attributes :id, :name, :model, :firmware, :scheme
  
  has_one :firmware
  has_one :scheme
end
