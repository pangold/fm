class Api::V1::LicenseHistorySerializer < Api::V1::BaseSerializer
  attributes :id, :operater, :product, :license, :firmware, :scheme

  has_one :operater
  has_one :product
  has_one :license
  has_one :firmware
  has_one :scheme
end
