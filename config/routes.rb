Rails.application.routes.draw do
  resources :license_codes
  resources :license_histories, only: [:index, :show]
  resources :burn_histories, only: [:index, :show]
  resources :product_histories, only: [:index, :show] do
    member do
      get 'examine'
      get 'unexamine'
    end
  end
  resources :syslogs, only: [:index, :show]
  resources :hosts
  resources :products
  resources :solutions
  resources :protocols
  resources :categories
  resources :licenses
  post 'licenses/invalid'
  post 'licenses/import'
  post 'licenses/export'
  post 'licenses/generate'
  resources :tools do
    member do
      get 'download'
    end
  end
  resources :upgrades do
    member do
      get 'download'
    end
  end
  resources :schemes do
    member do
      get 'download'
    end
  end
  resources :firmwares do
    member do
      get 'download'
    end
  end
  resources :attaches
  get 'contact/index'
  get 'about/index'
  get 'home/index'

  namespace :api do
    namespace :v1 do
      get 'products/model'
      get 'licenses/snid'
      get 'licenses/auto'
      resources :sessions, only: [:create, :destroy]
      resources :users, only: [:index, :show]
      resources :attaches, only: [:show]
      resources :firmwares, only: [:index, :show]
      resources :schemes, only: [:index, :show]
      resources :upgrades, only: [:index, :show]
      resources :tools, only: [:index, :show]
      resources :products, only: [:index, :show]
      resources :licenses, only: [:index, :show]
      resources :license_codes, only: [:index, :show]
      resources :burn_histories, only: [:index, :show, :create]
      resources :license_histories, only: [:index, :show, :create]
    end
  end
  
  devise_for :users
  resources :users, only: [:index, :show, :edit, :update, :destroy]
  post 'users/add_role'
  resources :roles
  root 'home#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
