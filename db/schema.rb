# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160202013312) do

  create_table "attaches", force: :cascade do |t|
    t.string   "file_name"
    t.string   "file_type"
    t.string   "extension"
    t.string   "save_name"
    t.string   "save_path"
    t.integer  "size"
    t.integer  "download_count"
    t.string   "file_hash"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "file_url"
    t.string   "original_name"
  end

  create_table "burn_histories", force: :cascade do |t|
    t.text     "description"
    t.integer  "operater_id"
    t.integer  "product_id"
    t.integer  "firmware_id"
    t.integer  "scheme_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "burn_histories", ["firmware_id"], name: "index_burn_histories_on_firmware_id"
  add_index "burn_histories", ["operater_id"], name: "index_burn_histories_on_operater_id"
  add_index "burn_histories", ["product_id"], name: "index_burn_histories_on_product_id"
  add_index "burn_histories", ["scheme_id"], name: "index_burn_histories_on_scheme_id"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "status"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "categories", ["category_id"], name: "index_categories_on_category_id"

  create_table "firmwares", force: :cascade do |t|
    t.string   "name"
    t.string   "version"
    t.text     "description"
    t.integer  "status"
    t.integer  "attach_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "firmwares", ["attach_id"], name: "index_firmwares_on_attach_id"

  create_table "hosts", force: :cascade do |t|
    t.string   "name"
    t.string   "ip"
    t.string   "dbname"
    t.string   "username"
    t.string   "password"
    t.string   "port"
    t.string   "address"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "license_codes", force: :cascade do |t|
    t.string   "code"
    t.text     "description"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "license_histories", force: :cascade do |t|
    t.text     "description"
    t.integer  "license_id"
    t.integer  "operater_id"
    t.integer  "product_id"
    t.integer  "firmware_id"
    t.integer  "scheme_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "license_histories", ["firmware_id"], name: "index_license_histories_on_firmware_id"
  add_index "license_histories", ["license_id"], name: "index_license_histories_on_license_id"
  add_index "license_histories", ["operater_id"], name: "index_license_histories_on_operater_id"
  add_index "license_histories", ["product_id"], name: "index_license_histories_on_product_id"
  add_index "license_histories", ["scheme_id"], name: "index_license_histories_on_scheme_id"

  create_table "licenses", force: :cascade do |t|
    t.string   "snid"
    t.string   "sn"
    t.integer  "point"
    t.integer  "flag"
    t.integer  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_histories", force: :cascade do |t|
    t.text     "description"
    t.integer  "status"
    t.integer  "operater_id"
    t.integer  "examiner_id"
    t.integer  "product_id"
    t.integer  "firmware_id"
    t.integer  "scheme_id"
    t.integer  "upgrade_id"
    t.integer  "tool_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "product_histories", ["examiner_id"], name: "index_product_histories_on_examiner_id"
  add_index "product_histories", ["firmware_id"], name: "index_product_histories_on_firmware_id"
  add_index "product_histories", ["operater_id"], name: "index_product_histories_on_operater_id"
  add_index "product_histories", ["product_id"], name: "index_product_histories_on_product_id"
  add_index "product_histories", ["scheme_id"], name: "index_product_histories_on_scheme_id"
  add_index "product_histories", ["tool_id"], name: "index_product_histories_on_tool_id"
  add_index "product_histories", ["upgrade_id"], name: "index_product_histories_on_upgrade_id"

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "model"
    t.string   "place"
    t.string   "param"
    t.boolean  "custom"
    t.integer  "status"
    t.integer  "category_id"
    t.integer  "protocol_id"
    t.integer  "solution_id"
    t.integer  "firmware_id"
    t.integer  "scheme_id"
    t.integer  "upgrade_id"
    t.integer  "tool_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id"
  add_index "products", ["firmware_id"], name: "index_products_on_firmware_id"
  add_index "products", ["protocol_id"], name: "index_products_on_protocol_id"
  add_index "products", ["scheme_id"], name: "index_products_on_scheme_id"
  add_index "products", ["solution_id"], name: "index_products_on_solution_id"
  add_index "products", ["tool_id"], name: "index_products_on_tool_id"
  add_index "products", ["upgrade_id"], name: "index_products_on_upgrade_id"

  create_table "protocols", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], name: "index_roles_on_name"

  create_table "schemes", force: :cascade do |t|
    t.string   "name"
    t.string   "version"
    t.text     "description"
    t.integer  "status"
    t.integer  "attach_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "schemes", ["attach_id"], name: "index_schemes_on_attach_id"

  create_table "solutions", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "syslogs", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "ip"
    t.integer  "operater_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "syslogs", ["operater_id"], name: "index_syslogs_on_operater_id"

  create_table "tools", force: :cascade do |t|
    t.string   "name"
    t.string   "version"
    t.text     "description"
    t.integer  "status"
    t.integer  "attach_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "tools", ["attach_id"], name: "index_tools_on_attach_id"

  create_table "upgrades", force: :cascade do |t|
    t.string   "name"
    t.string   "firmware_version"
    t.string   "scheme_version"
    t.string   "tool"
    t.text     "description"
    t.integer  "status"
    t.integer  "attach_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "upgrades", ["attach_id"], name: "index_upgrades_on_attach_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.string   "username"
    t.string   "authentication_token"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"

end
