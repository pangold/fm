class CreateHosts < ActiveRecord::Migration
  def change
    create_table :hosts do |t|
      t.string :name
      t.string :ip
      t.string :dbname
      t.string :username
      t.string :password
      t.string :port
      t.string :address
      t.integer :status

      t.timestamps null: false
    end
  end
end
