class CreateProductHistories < ActiveRecord::Migration
  def change
    create_table :product_histories do |t|
      t.text :description
      t.integer :status
      t.references :operater, index: true, foreign_key: true
      t.references :examiner, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.references :firmware, index: true, foreign_key: true
      t.references :scheme, index: true, foreign_key: true
      t.references :upgrade, index: true, foreign_key: true
      t.references :tool, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
