class CreateLicenseCodes < ActiveRecord::Migration
  def change
    create_table :license_codes do |t|
      t.string :code
      t.text :description
      t.integer :status

      t.timestamps null: false
    end
  end
end
