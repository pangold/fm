class CreateLicenses < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.string :snid
      t.string :sn
      t.integer :point
      t.integer :flag
      t.integer :status

      t.timestamps null: false
    end
  end
end
