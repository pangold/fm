class CreateAttaches < ActiveRecord::Migration
  def change
    create_table :attaches do |t|
      t.string :file_name
      t.string :file_type
      t.string :extension
      t.string :save_name
      t.string :save_path
      t.integer :size
      t.integer :download_count
      t.string :file_hash

      t.timestamps null: false
    end
  end
end
