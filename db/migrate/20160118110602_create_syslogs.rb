class CreateSyslogs < ActiveRecord::Migration
  def change
    create_table :syslogs do |t|
      t.string :title
      t.text :description
      t.string :ip
      t.references :operater, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
