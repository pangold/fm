class CreateBurnHistories < ActiveRecord::Migration
  def change
    create_table :burn_histories do |t|
      t.text :description
      t.references :operater, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.references :firmware, index: true, foreign_key: true
      t.references :scheme, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
