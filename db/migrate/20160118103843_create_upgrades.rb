class CreateUpgrades < ActiveRecord::Migration
  def change
    create_table :upgrades do |t|
      t.string :name
      t.string :firmware_version
      t.string :scheme_version
      t.string :tool
      t.text :description
      t.integer :status
      t.references :attach, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
