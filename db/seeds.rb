# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(name: '管理员', username: 'admin', email: 'admin@timelink.cn', password: 'timelink')
User.create(name: '亡灵法师', username: 'dora', email: 'pandora@timelink.cn', password: '11111111')
User.create(name: '普通人', username: 'normal', email: 'normal@timelink.cn', password: '11111111')

Role.create(name: 'admin')
Role.create(name: 'manager')
Role.create(name: 'member')

UsersRole.create(user_id: 1, role_id: 1)
UsersRole.create(user_id: 2, role_id: 2)
UsersRole.create(user_id: 3, role_id: 3)

